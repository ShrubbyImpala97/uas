// ubah menjadi fungsi dan prosedur

#include <iostream>
using namespace std;

char hitungHurufMutu(double nilai) {
    char Huruf_Mutu;
    if (nilai > 85 && nilai <= 100)
        Huruf_Mutu = 'A';
    else if (nilai > 70 && nilai <= 85)
        Huruf_Mutu = 'B';
    else if (nilai > 55 && nilai <= 70)
        Huruf_Mutu = 'C';
    else if (nilai > 40 && nilai <= 55)
        Huruf_Mutu = 'D';
    else if (nilai >= 0 && nilai <= 40)
        Huruf_Mutu = 'E';
    return Huruf_Mutu;
}

void tampilkanInformasiNilai(double absen, double tugas, double quiz, double uts, double uas) {
    cout << "Absen = " << absen << " UTS = " << uts << endl;
    cout << "Tugas = " << tugas << " UAS = " << uas << endl;
    cout << "Quiz = " << quiz << endl;
}

int main() {
    double nilai, quiz, absen, uts, uas, tugas;
    char Huruf_Mutu;

    quiz = 40; absen = 100; uts = 60; uas = 50; tugas = 80;

    tampilkanInformasiNilai(absen, tugas, quiz, uts, uas);

    nilai = ((0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas)) / 2;

    Huruf_Mutu = hitungHurufMutu(nilai);

    cout << "Huruf Mutu : " << Huruf_Mutu << endl;

    return 0;
}


//Pisahkan Code, sehingga user dapat mengubah milai dan merubah presentasi nilai


#include <iostream>
using namespace std;

char hitungHurufMutu(double nilai) {
    char Huruf_Mutu;
    if (nilai > 85 && nilai <= 100)
        Huruf_Mutu = 'A';
    else if (nilai > 70 && nilai <= 85)
        Huruf_Mutu = 'B';
    else if (nilai > 55 && nilai <= 70)
        Huruf_Mutu = 'C';
    else if (nilai > 40 && nilai <= 55)
        Huruf_Mutu = 'D';
    else if (nilai >= 0 && nilai <= 40)
        Huruf_Mutu = 'E';
    return Huruf_Mutu;
}

void tampilkanInformasiNilai(double absen, double tugas, double quiz, double uts, double uas) {
    cout << "Absen = " << absen << " UTS = " << uts << endl;
    cout << "Tugas = " << tugas << " UAS = " << uas << endl;
    cout << "Quiz = " << quiz << endl;
}

int main() {
    double nilai, quiz, absen, uts, uas, tugas;
    char Huruf_Mutu;

    cout << "Masukkan nilai Absen: ";
    cin >> absen;
    cout << "Masukkan nilai Tugas: ";
    cin >> tugas;
    cout << "Masukkan nilai Quiz: ";
    cin >> quiz;
    cout << "Masukkan nilai UTS: ";
    cin >> uts;
    cout << "Masukkan nilai UAS: ";
    cin >> uas;

    tampilkanInformasiNilai(absen, tugas, quiz, uts, uas);

    nilai = ((0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas)) / 2;

    Huruf_Mutu = hitungHurufMutu(nilai);

    cout << "Huruf Mutu : " << Huruf_Mutu << endl;

    return 0;
}


// Ubah code agar bisa mngolah data lebih dari satu mahasiswa

#include <iostream>
using namespace std;

char hitungHurufMutu(double nilai) {
    char Huruf_Mutu;
    if (nilai > 85 && nilai <= 100)
        Huruf_Mutu = 'A';
    else if (nilai > 70 && nilai <= 85)
        Huruf_Mutu = 'B';
    else if (nilai > 55 && nilai <= 70)
        Huruf_Mutu = 'C';
    else if (nilai > 40 && nilai <= 55)
        Huruf_Mutu = 'D';
    else if (nilai >= 0 && nilai <= 40)
        Huruf_Mutu = 'E';
    return Huruf_Mutu;
}

void tampilkanInformasiNilai(double absen, double tugas, double quiz, double uts, double uas) {
    cout << "Absen = " << absen << " UTS = " << uts << endl;
    cout << "Tugas = " << tugas << " UAS = " << uas << endl;
    cout << "Quiz = " << quiz << endl;
}

int main() {
    const int jumlahMahasiswa = 3; // Jumlah mahasiswa
    double nilai[jumlahMahasiswa], quiz[jumlahMahasiswa], absen[jumlahMahasiswa], uts[jumlahMahasiswa], uas[jumlahMahasiswa], tugas[jumlahMahasiswa];
    char Huruf_Mutu[jumlahMahasiswa];

    for (int i = 0; i < jumlahMahasiswa; ++i) {
        cout << "Masukkan nilai untuk Mahasiswa " << i + 1 << ":" << endl;
        cout << "Absen: ";
        cin >> absen[i];
        cout << "Tugas: ";
        cin >> tugas[i];
        cout << "Quiz: ";
        cin >> quiz[i];
        cout << "UTS: ";
        cin >> uts[i];
        cout << "UAS: ";
        cin >> uas[i];
    }

    for (int i = 0; i < jumlahMahasiswa; ++i) {
        tampilkanInformasiNilai(absen[i], tugas[i], quiz[i], uts[i], uas[i]);

        nilai[i] = ((0.1 * absen[i]) + (0.2 * tugas[i]) + (0.3 * quiz[i]) + (0.4 * uts[i]) + (0.5 * uas[i])) / 2;

        Huruf_Mutu[i] = hitungHurufMutu(nilai[i]);

        cout << "Huruf Mutu untuk Mahasiswa " << i + 1 << " : " << Huruf_Mutu[i] << endl;
    }

    return 0;
}
